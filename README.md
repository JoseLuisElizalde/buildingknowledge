# Ejercicios completos de libros relacionados con la programación

![alt tag](https://ibb.co/bEL620)

Varios ejemplos de código que se presentan a lo largo de los libros presentados.


## Lista de Ejercicios
| Nombre del Ejercicio                           |        Autor      |  Descripcion   |    Estado    |
| -----------------------------------------------|-------------------|----------------|--------------|
| bk001_El_camino_a_un_mejor_programador         | Esteban Manchado Velázquez, Joaquín Caraballo Moreno, Yeray Darias Camacho                  | Ejercicios variados. | Pendiente  |
| bk002_El_Lenguaje_Python                       | David Masip Rodon | Ejercicios de python, concluyendo en ejercicios de Inteligencia Artificial.|  Pendiente  |



