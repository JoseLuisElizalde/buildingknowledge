#Ejemplo de cadenas de caracteres

a='hola\n'
b='\tEste es un ejempp\blo de cadenas de caracteres\n'
c="""Es posible escribir saltos de linea
sin necesidad de codigos de escape"""

print a,b,c

d=a+b; #Concatenacion
e="repite"
f=3*e; #Repeticion
g=e*3; #Equivalente al caso anterior

print d,f,g