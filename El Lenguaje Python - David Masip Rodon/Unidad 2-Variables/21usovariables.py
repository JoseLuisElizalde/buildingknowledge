#Ejemplos de variables

a=42; #Valor enterro
along = 42l; #Valor entero long (32-64 bits en funcion de la informacion)
ahex=0x2a;  #a en notacion hexadecimal 
aoctal=052; #a en notacion octal 

b=3.1416; #Valoe en coma flotante
bnotacion=3.14e0;
c=3+7j; #Python soporta numeros complejos
d="Ejemplo de cadena de caracteres"; #Una cadena de caracteres

#Imprimir las variables por pantalla
print a,b,c,d; 

#Tipo de las variables
type(a);
type(b);
type(c);
type(d);
