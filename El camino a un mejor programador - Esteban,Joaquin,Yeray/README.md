# El Camino a Un Mejor Programador

![alt tag](https://olcovers2.blob.core.windows.net/coverswp/2012/11/El-camino-a-un-mejor-programador-OpenLibra-300x425.jpg)

Prologo

Se cuenta que una vez le preguntaron a Miguel Angel como había procedido para esculpir el David. Su respuesta fue, que lo que había hecho era,
simplemente, eliminar del bloque de piedra original todo lo que sobraba. Independientemente de la verosimilitud de esa anécdota, podemos encontrar
 en ella la enseñanza de una actitud a tomar para abordar problemas prácticos de desarrollo de ingeniería.


## Lista de Capitulos
| Capitulos                            | Descripcion   |
| -------------------------------------|---------------|
| Capitulo 1                           | Lecciones de aprender un lenguaje funcional.
| Capitulo 2                           | Documentación activa.
| Capitulo 3                           | Siete problemas al probar programas.
| Capitulo 4                           | Calidad en software.
| Capitulo 5                           | Integración continua.
| Capitulo 6                           | Desarrollo dirigido por pruebas.





